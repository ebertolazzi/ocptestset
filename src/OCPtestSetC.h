/*--------------------------------------------------------------------------*\
 |                                                                          |
 |  Copyright 2016                                                          |                                                                          |
 |                                                                          |
 |  Enrico Bertolazzi^(*)  and  Matthias Gerdts^(**) (Ingenieurmathematik)  |
 |                                                                          |
 |  (*) Department of Industrial Engineering                                |
 |      University of Trento                                                |
 |      email: enrico.bertolazzi@unitn.it                                   |
 |                                                                          |
 | (**) Institut fuer Mathematik und Rechneranwendung                       |
 |      Fakultaet fuer Luftund Raumfahrttechnik                             |
 |      Universitaet der Bundeswehr Muenchen                                |
 |      email: matthias.gerdts@unibw.de                                     |
 |                                                                          |
 |  Licensed under the EUPL, Version 1.1 or – as soon they will be          |
 |  approved by the European Commission - subsequent versions of the EUPL   |
 |  (the "Licence"); You may not use this work except in compliance with    |
 |  the Licence.                                                            |
 |  You may obtain a copy of the Licence at:                                |
 |                                                                          |
 |  http://ec.europa.eu/idabc/eupl5                                         |
 |                                                                          |
 |  Unless required by applicable law or agreed to in writing, software     |
 |  distributed under the Licence is distributed on an "AS IS" basis,       |
 |  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or         |
 |  implied.                                                                |
 |  See the Licence for the specific language governing permissions and     |
 |  limitations under the Licence.                                          |
 |                                                                          |
\*--------------------------------------------------------------------------*/

#ifndef OCP_TEST_SET
#define OCP_TEST_SET

#ifdef __cplusplus
extern "C" {
#endif

typedef double OCP_real ;
typedef int    OCP_integer ;

/*!
  Read a file with the description of the problem in the format XXX.
  
  \return 0 ok otherwise the code ...
\*/

OCP_integer
OCP_load_problem( char const problem_name[] ) ;


/*!
  Return the size of the loaded problem
  
  \return 0 ok otherwise the code ...
\*/
OCP_integer
OCP_get_dimension( OCP_integer & number_of_differential_states,
                   OCP_integer & number_of_algebraic_states,
                   OCP_integer & number_of_controls,
                   OCP_integer & number_of_discrete_controls,
                   OCP_integer & number_of_parameters,
                   OCP_integer & number_of_boundary_conditions,
                   OCP_integer & number_of_states_constraints ) ;

/*!
  \return 0 ok otherwise the code ...
\*/
OCP_integer
OCP_differential_state_name( OCP_integer i, char name[], OCP_integer maxLen );

/*!
  \return 0 ok otherwise the code ...
\*/
OCP_integer
OCP_algebraic_state_name( OCP_integer i, char name[], OCP_integer maxLen );

/*!
  \return 0 ok otherwise the code ...
\*/
OCP_integer
OCP_control_name( OCP_integer i, char name[], OCP_integer maxLen );

/*!
  \return 0 ok otherwise the code ...
\*/
OCP_integer
OCP_discrete_control_name( OCP_integer i, char name[], OCP_integer maxLen );

/*!
  \return 0 ok otherwise the code ...
\*/
OCP_integer
OCP_parameter_name( OCP_integer i, char name[], OCP_integer maxLen );

/*!
  \return 0 ok otherwise the code ...
\*/
OCP_integer
OCP_boundary_condition_name( OCP_integer i, char name[], OCP_integer maxLen );

/*!
  \return 0 ok otherwise the code ...
\*/
OCP_integer
OCP_states_constraint_name( OCP_integer i, char name[], OCP_integer maxLen );

/*!
  \return 0 ok otherwise the code ...
\*/
OCP_integer
OCP_get_differential_states_bounds( OCP_real lower[], OCP_real upper[] ) ;

/*!
  \return 0 ok otherwise the code ...
\*/
OCP_integer
OCP_get_algebraic_states_bounds( OCP_real lower[], OCP_real upper[] ) ;

/*!
  \return 0 ok otherwise the code ...
\*/
OCP_integer
OCP_get_controls_bounds( OCP_real lower[], OCP_real upper[] ) ;
/*!
  \return 0 ok otherwise the code ...
\*/
OCP_integer
OCP_get_discrete_control_values( OCP_integer i, OCP_real values[] ) ;

OCP_integer
OCP_get_discrete_control_number_of_values( OCP_integer i, OCP_integer & number_of_values ) ;

/*!
  \return 0 ok otherwise the code ...
\*/
OCP_integer
OCP_get_parameters_bounds( OCP_real lower[], OCP_real upper[] ) ;

/*!
  \return 0 ok otherwise the code ...
\*/
OCP_integer
OCP_get_boundary_conditions_bounds( OCP_real lower[], OCP_real upper[] ) ;

/*!
  \return 0 ok otherwise the code ...
\*/
OCP_integer
OCP_get_states_constraints_bounds( OCP_real lower[], OCP_real upper[] ) ;

/*!
  Return the value of Mayer term...
  
  \param computation_mode 1 compute only value
                          2 compute only gradient
                          3 compute both value and gradient
\*/
OCP_integer
OCP_mayer( OCP_real    const initial_state[],
           OCP_real    const final_state[],
           OCP_real    const parameters[],
           OCP_integer const computation_mode,
           OCP_real &        value,
           OCP_real          gradient[] ) ; // initial + final + parameters
  
// -----------------------------------------------------------------------------

/*!
  \return 0 ok otherwise the code ...
\*/
OCP_integer
OCP_lagrange( OCP_real    const differential_states[],
              OCP_real    const algebraic_states[],
              OCP_real    const controls[],
              OCP_real    const discrete_controls[],
              OCP_real    const parameters[],
              OCP_real          independent, // normally time
              OCP_integer const computation_mode,
              OCP_real &        value,
              OCP_real          gradient[] ) ;  // differential + algebraic + controls + parameters + independent

// -----------------------------------------------------------------------------

/*!
  \return 0 ok otherwise the code ...
\*/
OCP_integer
OCP_rhs_differental_part_nnz( OCP_integer & nnz ) ;

/*!
  \return 0 ok otherwise the code ...
\*/
OCP_integer
OCP_rhs_differental_part_pattern( OCP_integer rows[],
                                  OCP_integer cols[],
                                  OCP_integer offset ) ;

/*!
  \return 0 ok otherwise the code ...
\*/
OCP_integer
OCP_rhs_differental_part( OCP_real    const differential_states[],
                          OCP_real    const algebraic_states[],
                          OCP_real    const controls[],
                          OCP_real    const discrete_controls[],
                          OCP_real    const parameters[],
                          OCP_real          independent, // normally time
                          OCP_integer const computation_mode,
                          OCP_real          rhs[],
                          OCP_real          jacobian_values_of_rhs[] ) ;
                          // differential + algebraic + controls + parameters + independent

// -----------------------------------------------------------------------------

/*!
  \return 0 ok otherwise the code ...
\*/
OCP_integer
OCP_rhs_algebraic_part_nnz( OCP_integer & nnz ) ;

/*!
  \return 0 ok otherwise the code ...
\*/
OCP_integer
OCP_rhs_algebraic_part_pattern( OCP_integer rows[],
                                OCP_integer cols[],
                                OCP_integer offset ) ;

/*!
  \return 0 ok otherwise the code ...
\*/
OCP_integer
OCP_rhs_algebraic_part( OCP_real    const differential_states[],
                        OCP_real    const algebraic_states[],
                        OCP_real    const controls[],
                        OCP_real    const discrete_controls[],
                        OCP_real    const parameters[],
                        OCP_real          independent, // normally time
                        OCP_integer const computation_mode,
                        OCP_real          rhs[],
                        OCP_real          jacobian_values_of_rhs[] ) ;
                        // differential + algebraic + controls + parameters + independent

// -----------------------------------------------------------------------------

/*!
  \return 0 ok otherwise the code ...
\*/
OCP_integer
OCP_lhs_mass_matrix_nnz( OCP_integer & nnz ) ;

/*!
  \return 0 ok otherwise the code ...
\*/
OCP_integer
OCP_lhs_mass_matrix_pattern( OCP_integer rows[],
                             OCP_integer cols[],
                             OCP_integer offset ) ;

/*!
  \return 0 ok otherwise the code ...
\*/
OCP_integer
OCP_lhs_mass_matrix( OCP_real const differential_states[],
                     OCP_real const parameters[],
                     OCP_real       independent, // normally time
                     OCP_real       values[] ) ;

// -----------------------------------------------------------------------------

/*!
  \return 0 ok otherwise the code ...
\*/
OCP_integer
OCP_lhs_adjoint_mass_matrix_derivative_nnz( OCP_integer & nnz ) ;

/*!
  \return 0 ok otherwise the code ...
\*/
OCP_integer
OCP_lhs_adjoint_mass_matrix_derivative_pattern( OCP_integer rows[],
                                                OCP_integer cols[],
                                                OCP_integer offset ) ;

/*!
  \return 0 ok otherwise the code ...
\*/
OCP_integer
OCP_lhs_adjoint_mass_matrix_derivative( OCP_real const differential_states[],
                                        OCP_real const parameters[],
                                        OCP_real       independent, // normally time
                                        OCP_real       values[] ) ;

// -----------------------------------------------------------------------------

/*!
  \return 0 ok otherwise the code ...
\*/
OCP_integer
OCP_states_constraints_nnz( OCP_integer & nnz ) ;

/*!
  \return 0 ok otherwise the code ...
\*/
OCP_integer
OCP_states_constraints_pattern( OCP_integer rows[],
                                OCP_integer cols[],
                                OCP_integer offset ) ;

/*!
  \return 0 ok otherwise the code ...
\*/
OCP_integer
OCP_states_constraints( OCP_real    const differential_states[],
                        OCP_real    const algebraic_states[],
                        OCP_real    const controls[],
                        OCP_real    const discrete_controls[],
                        OCP_real    const parameters[],
                        OCP_real          independent, // normally time
                        OCP_integer const computation_mode,
                        OCP_real          constraints[],
                        OCP_real          jacobian_values_of_constraints[] ) ;
                        // differential + algebraic + controls + parameters + independent

// -----------------------------------------------------------------------------

/*!
  \return 0 ok otherwise the code ...
\*/
OCP_integer
OCP_boundary_conditions_nnz( OCP_integer & nnz ) ;

/*!
  \return 0 ok otherwise the code ...
\*/
OCP_integer
OCP_boundary_conditions_pattern( OCP_integer rows[],
                                 OCP_integer cols[],
                                 OCP_integer offset ) ;

/*!
  \return 0 ok otherwise the code ...
\*/
OCP_integer
OCP_boundary_conditions( OCP_real    const initial_state[],
                         OCP_real    const final_state[],
                         OCP_real    const parameters[],
                         OCP_integer const computation_mode,
                         OCP_real          values[],
                         OCP_real          jacobian_values_of_bc[] ) ;
                        // differential + algebraic + controls + parameters + independent

#ifdef __cplusplus
}
#endif

#endif
