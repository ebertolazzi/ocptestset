/*--------------------------------------------------------------------------*\
 |                                                                          |
 |  Copyright 2016                                                          |                                                                          |
 |                                                                          |
 |  Enrico Bertolazzi^(*)  and  Matthias Gerdts^(**) (Ingenieurmathematik)  |
 |                                                                          |
 |  (*) Department of Industrial Engineering                                |
 |      University of Trento                                                |
 |      email: enrico.bertolazzi@unitn.it                                   |
 |                                                                          |
 | (**) Institut fuer Mathematik und Rechneranwendung                       |
 |      Fakultaet fuer Luftund Raumfahrttechnik                             |
 |      Universitaet der Bundeswehr Muenchen                                |
 |      email: matthias.gerdts@unibw.de                                     |
 |                                                                          |
 |  Licensed under the EUPL, Version 1.1 or – as soon they will be          |
 |  approved by the European Commission - subsequent versions of the EUPL   |
 |  (the "Licence"); You may not use this work except in compliance with    |
 |  the Licence.                                                            |
 |  You may obtain a copy of the Licence at:                                |
 |                                                                          |
 |  http://ec.europa.eu/idabc/eupl5                                         |
 |                                                                          |
 |  Unless required by applicable law or agreed to in writing, software     |
 |  distributed under the Licence is distributed on an "AS IS" basis,       |
 |  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or         |
 |  implied.                                                                |
 |  See the Licence for the specific language governing permissions and     |
 |  limitations under the Licence.                                          |
 |                                                                          |
\*--------------------------------------------------------------------------*/

#ifndef OCP_TEST_SET_CPP_HH
#define OCP_TEST_SET_CPP_HH

#include <string>
#include <vector>
#include <iostream>
#include <sstream>
#include <iomanip>
#include <stdexcept>

#ifndef OCP_TEST_ERROR
  #define OCP_TEST_ERROR(MSG) { \
    std::ostringstream ost ; ost << MSG << '\n' ; \
    throw std::runtime_error(ost.str()) ; \
  }
#endif

#ifndef OCP_TEST_ASSERT
  #define OCP_TEST_ASSERT(COND,MSG) if ( !(COND) ) OCP_TEST_ERROR( MSG )
#endif

namespace OCP_test_set {

  using namespace std ;

  typedef double real ;
  typedef int    integer ;

  /*!
    Read a file with the description of the problem in the format XXX.
   
    \return 0 ok otherwise the code ...
  \*/
  
  class Test {

    string const _problem_name ;
    
  protected:

    integer dim_x  ; //!< number of differential states
    integer dim_y  ; //!< number of algebraic states
    integer dim_u  ; //!< number of controls
    integer dim_s  ; //!< number of discrete controls
    integer dim_p  ; //!< number of parameters
    integer dim_bc ; //!< number of boundary conditions
    integer dim_c  ; //!< number of states constraints
    
    vector<string> _differential_state_name ;
    vector<string> _algebraic_state_name ;
    vector<string> _control_name ;
    vector<string> _discrete_control_name ;
    vector<string> _parameter_name ;
    vector<string> _boundary_condition_name ;
    vector<string> _states_constraint_name ;

  public:

    integer
    load( char const problem_name[] ) ;

    //*! \return 0 ok otherwise the code
    integer number_of_differential_states() const { return dim_x  ; }
    integer number_of_algebraic_states()    const { return dim_u  ; }
    integer number_of_controls()            const { return dim_u  ; }
    integer number_of_discrete_controls()   const { return dim_s  ; }
    integer number_of_parameters()          const { return dim_p  ; }
    integer number_of_boundary_conditions() const { return dim_bc ; }
    integer number_of_states_constraints()  const { return dim_c  ; }

    string differential_state_name( integer i ) const ;
    string algebraic_state_name( integer i )    const ;
    string control_name( integer i )            const ;
    string discrete_control_name( integer i )   const ;
    string parameter_name( integer i )          const ;
    string boundary_condition_name( integer i ) const ;
    string states_constraint_name( integer i )  const ;

    virtual
    void
    get_differential_states_bounds( vector<real> & lower,
                                    vector<real> & upper ) const = 0 ;

    virtual
    void
    get_algebraic_states_bounds( vector<real> & lower,
                                 vector<real> & upper ) const = 0 ;

    virtual
    void
    get_controls_bounds( vector<real> & lower,
                         vector<real> & upper ) const = 0 ;

    virtual
    void
    get_discrete_control_values( integer i, vector<real> & values ) const = 0 ;

    virtual
    integer
    get_discrete_control_number_of_values( integer i ) const  ;

    virtual
    void
    get_parameters_bounds( vector<real> & lower,
                           vector<real> & upper ) const = 0 ;

    virtual
    void
    get_boundary_conditions_bounds( vector<real> & lower,
                                    vector<real> & upper ) const = 0 ;

    virtual
    void
    get_states_constraints_bounds( vector<real> & lower,
                                   vector<real> & upper ) const = 0 ;
    /*!
      Return the value of Mayer term...
  
      \param computation_mode 1 compute only value
                              2 compute only gradient
                              3 compute both value and gradient
    \*/
    virtual
    integer
    mayer( real    const initial_state[],
           real    const final_state[],
           real    const parameters[],
           integer const computation_mode,
           real &        value,
           real          gradient[] ) const = 0 ; // initial + final + parameters
  
    /*!
      \return 0 ok otherwise the code ...
    \*/
    virtual
    integer
    lagrange( real    const differential_states[],
              real    const algebraic_states[],
              real    const controls[],
              real    const discrete_controls[],
              real    const parameters[],
              real          independent, // normally time
              integer const computation_mode,
              real &        value,
              real          gradient[] ) const = 0 ;  // differential + algebraic + controls + parameters + independent

    /*!
      \return 0 ok otherwise the code ...
    \*/
    virtual
    integer
    rhs_differental_part_nnz() const = 0 ;

    /*!
      \return 0 ok otherwise the code ...
    \*/
    virtual
    void
    rhs_differental_part_pattern( integer rows[],
                                  integer cols[],
                                  integer offset ) const = 0  ;

    /*!
     \return 0 ok otherwise the code ...
    \*/
    virtual
    integer
    rhs_differental_part( real    const differential_states[],
                          real    const algebraic_states[],
                          real    const controls[],
                          real    const discrete_controls[],
                          real    const parameters[],
                          real          independent, // normally time
                          integer const computation_mode,
                          real          rhs[],
                          real          jacobian_values_of_rhs[] ) const = 0  ;
                          // differential + algebraic + controls + parameters + independent

    /*!
      \return 0 ok otherwise the code ...
    \*/
    virtual
    integer
    rhs_algebraic_part_nnz() const = 0 ;

    /*!
      \return 0 ok otherwise the code ...
    \*/
    virtual
    void
    rhs_algebraic_part_pattern( integer rows[],
                                integer cols[],
                                integer offset ) const = 0 ;

    /*!
      \return 0 ok otherwise the code ...
    \*/
    virtual
    integer
    rhs_algebraic_part( real    const differential_states[],
                        real    const algebraic_states[],
                        real    const controls[],
                        real    const discrete_controls[],
                        real    const parameters[],
                        real          independent, // normally time
                        integer const computation_mode,
                        real          rhs[],
                        real          jacobian_values_of_rhs[] ) const = 0 ;
                        // differential + algebraic + controls + parameters + independent
    /*!
      \return 0 ok otherwise the code ...
    \*/
    virtual
    integer
    lhs_mass_matrix_nnz() const = 0 ;

    /*!
      \return 0 ok otherwise the code ...
    \*/
    virtual
    void
    lhs_mass_matrix_pattern( integer rows[],
                             integer cols[],
                             integer offset ) const = 0 ;

    /*!
      \return 0 ok otherwise the code ...
    \*/
    virtual
    integer
    lhs_mass_matrix( real const differential_states[],
                     real const parameters[],
                     real       independent, // normally time
                     real       values[] ) const = 0 ;

    /*!
      \return 0 ok otherwise the code ...
    \*/
    virtual
    integer
    lhs_adjoint_mass_matrix_derivative_nnz() const = 0 ;

    /*!
      \return 0 ok otherwise the code ...
    \*/
    virtual
    void
    lhs_adjoint_mass_matrix_derivative_pattern( integer rows[],
                                                integer cols[],
                                                integer offset ) const = 0 ;

    /*!
      \return 0 ok otherwise the code ...
    \*/
    virtual
    integer
    lhs_adjoint_mass_matrix_derivative( real const differential_states[],
                                        real const parameters[],
                                        real       independent, // normally time
                                        real       values[] ) const = 0 ;
    /*!
      \return 0 ok otherwise the code ...
    \*/
    virtual
    integer
    states_constraints_nnz() const = 0 ;

    /*!
      \return 0 ok otherwise the code ...
    \*/
    virtual
    void
    states_constraints_pattern( integer rows[],
                                integer cols[],
                                integer offset ) const = 0 ;

    /*!
      \return 0 ok otherwise the code ...
    \*/
    virtual
    integer
    states_constraints( real    const differential_states[],
                        real    const algebraic_states[],
                        real    const controls[],
                        real    const discrete_controls[],
                        real    const parameters[],
                        real          independent, // normally time
                        integer const computation_mode,
                        real          constraints[],
                        real          jacobian_values_of_constraints[] ) const = 0 ;
                        // differential + algebraic + controls + parameters + independent

    /*!
      \return 0 ok otherwise the code ...
    \*/
    virtual
    integer
    boundary_conditions_nnz() const = 0 ;

    /*!
      \return 0 ok otherwise the code ...
    \*/
    virtual
    void
    boundary_conditions_pattern( integer rows[],
                                 integer cols[],
                                 integer offset ) const = 0 ;

    /*!
      \return 0 ok otherwise the code ...
    \*/
    virtual
    integer
    boundary_conditions( real    const initial_state[],
                         real    const final_state[],
                         real    const parameters[],
                         integer const computation_mode,
                         real          values[],
                         real          jacobian_values_of_bc[] ) const = 0 ;
  } ;
}

#endif
