/*--------------------------------------------------------------------------*\
 |                                                                          |
 |  Copyright 2016                                                          |                                                                          |
 |                                                                          |
 |  Enrico Bertolazzi^(*)  and  Matthias Gerdts^(**) (Ingenieurmathematik)  |
 |                                                                          |
 |  (*) Department of Industrial Engineering                                |
 |      University of Trento                                                |
 |      email: enrico.bertolazzi@unitn.it                                   |
 |                                                                          |
 | (**) Institut fuer Mathematik und Rechneranwendung                       |
 |      Fakultaet fuer Luftund Raumfahrttechnik                             |
 |      Universitaet der Bundeswehr Muenchen                                |
 |      email: matthias.gerdts@unibw.de                                     |
 |                                                                          |
 |  Licensed under the EUPL, Version 1.1 or – as soon they will be          |
 |  approved by the European Commission - subsequent versions of the EUPL   |
 |  (the "Licence"); You may not use this work except in compliance with    |
 |  the Licence.                                                            |
 |  You may obtain a copy of the Licence at:                                |
 |                                                                          |
 |  http://ec.europa.eu/idabc/eupl5                                         |
 |                                                                          |
 |  Unless required by applicable law or agreed to in writing, software     |
 |  distributed under the Licence is distributed on an "AS IS" basis,       |
 |  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or         |
 |  implied.                                                                |
 |  See the Licence for the specific language governing permissions and     |
 |  limitations under the Licence.                                          |
 |                                                                          |
\*--------------------------------------------------------------------------*/

#include <string>
#include <vector>
#include "OCPtestSetCpp.hh"

namespace OCP_test_set {

  string
  Test::differential_state_name( integer i ) const {
    OCP_TEST_ASSERT( i >= 0 && i < dim_x,
                     "in Test::differential_state_name( " << i << " ) index out of range: [ 0.." <<
                     dim_x-1 << "]" ) ;
    return _differential_state_name[i] ;
  }

  string
  Test::algebraic_state_name( integer i ) const {
    OCP_TEST_ASSERT( i >= 0 && i < dim_y,
                     "in Test::algebraic_state_name( " << i << " ) index out of range: [ 0.." <<
                     dim_y-1 << "]" ) ;
    return _algebraic_state_name[i] ;
  }

  string
  Test::control_name( integer i ) const {
    OCP_TEST_ASSERT( i >= 0 && i < dim_u,
                     "in Test::control_name( " << i << " ) index out of range: [ 0.." <<
                     dim_u-1 << "]" ) ;
    return _control_name[i] ;
  }

  string
  Test::discrete_control_name( integer i ) const {
    OCP_TEST_ASSERT( i >= 0 && i < dim_s,
                     "in Test::discrete_control_name( " << i << " ) index out of range: [ 0.." <<
                     dim_s-1 << "]" ) ;
    return _discrete_control_name[i] ;
  }

  string
  Test::parameter_name( integer i ) const {
    OCP_TEST_ASSERT( i >= 0 && i < dim_p,
                     "in Test::parameter_name( " << i << " ) index out of range: [ 0.." <<
                     dim_p-1 << "]" ) ;
    return _parameter_name[i] ;
  }

  string
  Test::boundary_condition_name( integer i ) const {
    OCP_TEST_ASSERT( i >= 0 && i < dim_bc,
                     "in Test::boundary_condition_name( " << i << " ) index out of range: [ 0.." <<
                     dim_bc-1 << "]" ) ;
    return _boundary_condition_name[i] ;
  }

  string
  Test::states_constraint_name( integer i ) const {
    OCP_TEST_ASSERT( i >= 0 && i < dim_c,
                     "in Test::states_constraint_name( " << i << " ) index out of range: [ 0.." <<
                     dim_c-1 << "]" ) ;
    return _states_constraint_name[i] ;
  }

  integer
  Test::load( char const problem_name[] ) {
    // to be finished
    return 0 ;
  }
}
