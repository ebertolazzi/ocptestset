/*--------------------------------------------------------------------------*\
 |                                                                          |
 |  Copyright 2016                                                          |                                                                          |
 |                                                                          |
 |  Enrico Bertolazzi^(*)  and  Matthias Gerdts^(**) (Ingenieurmathematik)  |
 |                                                                          |
 |  (*) Department of Industrial Engineering                                |
 |      University of Trento                                                |
 |      email: enrico.bertolazzi@unitn.it                                   |
 |                                                                          |
 | (**) Institut fuer Mathematik und Rechneranwendung                       |
 |      Fakultaet fuer Luftund Raumfahrttechnik                             |
 |      Universitaet der Bundeswehr Muenchen                                |
 |      email: matthias.gerdts@unibw.de                                     |
 |                                                                          |
 |  Licensed under the EUPL, Version 1.1 or – as soon they will be          |
 |  approved by the European Commission - subsequent versions of the EUPL   |
 |  (the "Licence"); You may not use this work except in compliance with    |
 |  the Licence.                                                            |
 |  You may obtain a copy of the Licence at:                                |
 |                                                                          |
 |  http://ec.europa.eu/idabc/eupl5                                         |
 |                                                                          |
 |  Unless required by applicable law or agreed to in writing, software     |
 |  distributed under the Licence is distributed on an "AS IS" basis,       |
 |  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or         |
 |  implied.                                                                |
 |  See the Licence for the specific language governing permissions and     |
 |  limitations under the Licence.                                          |
 |                                                                          |
\*--------------------------------------------------------------------------*/

#include "OCPtestSetC.h"
#include "OCPtestSetCpp.hh"

using namespace OCP_test_set ;


static Test * pTest ;

/*!
  Read a file with the description of the problem in the format XXX.
  
  \return 0 ok otherwise the code ...
\*/

OCP_integer
OCP_load_problem( char const problem_name[] ) {
  return 0 ;
}

/*!
  Return the size of the loaded problem
  
  \return 0 ok otherwise the code ...
\*/
OCP_integer
OCP_get_dimension( OCP_integer & number_of_differential_states,
                   OCP_integer & number_of_algebraic_states,
                   OCP_integer & number_of_controls,
                   OCP_integer & number_of_discrete_controls,
                   OCP_integer & number_of_parameters,
                   OCP_integer & number_of_boundary_conditions,
                   OCP_integer & number_of_states_constraints ) {
  number_of_differential_states = pTest->number_of_differential_states() ;
  number_of_algebraic_states    = pTest->number_of_algebraic_states() ;
  number_of_controls            = pTest->number_of_controls() ;
  number_of_discrete_controls   = pTest->number_of_discrete_controls() ;
  number_of_parameters          = pTest->number_of_parameters() ;
  number_of_boundary_conditions = pTest->number_of_boundary_conditions() ;
  number_of_states_constraints  = pTest->number_of_states_constraints() ;
  return 0 ;
}

/*!
  \return 0 ok otherwise the code ...
\*/
OCP_integer
OCP_differential_state_name( OCP_integer i, char name[] ) {
  string tmp = pTest->differential_state_name( i ) ;
  std::copy( tmp.begin(), tmp.end(), name ) ;
  name[tmp.length()] = '\0' ;
  return 0 ;
}

/*!
  \return 0 ok otherwise the code ...
\*/
OCP_integer
OCP_algebraic_state_name( OCP_integer i, char name[] ) {
  string tmp = pTest->algebraic_state_name( i ) ;
  std::copy( tmp.begin(), tmp.end(), name ) ;
  name[tmp.length()] = '\0' ;
  return 0 ;
}

/*!
  \return 0 ok otherwise the code ...
\*/
OCP_integer
OCP_control_name( OCP_integer i, char name[] ) {
  string tmp = pTest->control_name( i ) ;
  std::copy( tmp.begin(), tmp.end(), name ) ;
  name[tmp.length()] = '\0' ;
  return 0 ;
}

/*!
  \return 0 ok otherwise the code ...
\*/
OCP_integer
OCP_discrete_control_name( OCP_integer i, char name[] ) {
  string tmp = pTest->discrete_control_name( i ) ;
  std::copy( tmp.begin(), tmp.end(), name ) ;
  name[tmp.length()] = '\0' ;
  return 0 ;
}

/*!
  \return 0 ok otherwise the code ...
\*/
OCP_integer
OCP_parameter_name( OCP_integer i, char name[] ) {
  string tmp = pTest->parameter_name( i ) ;
  std::copy( tmp.begin(), tmp.end(), name ) ;
  name[tmp.length()] = '\0' ;
  return 0 ;
}

/*!
  \return 0 ok otherwise the code ...
\*/
OCP_integer
OCP_boundary_condition_name( OCP_integer i, char name[] ) {
  string tmp = pTest->boundary_condition_name( i ) ;
  std::copy( tmp.begin(), tmp.end(), name ) ;
  name[tmp.length()] = '\0' ;
  return 0 ;
}

/*!
  \return 0 ok otherwise the code ...
\*/
OCP_integer
OCP_states_constraint_name( OCP_integer i, char name[] ) {
  string tmp = pTest->states_constraint_name( i ) ;
  std::copy( tmp.begin(), tmp.end(), name ) ;
  name[tmp.length()] = '\0' ;
  return 0 ;
}

/*!
  \return 0 ok otherwise the code ...
\*/
OCP_integer
OCP_get_differential_states_bounds( OCP_real lower[], OCP_real upper[] ) {
  vector<real> L, U ;
  pTest->get_differential_states_bounds( L, U ) ;
  std::copy( L.begin(), L.end(), lower ) ;
  std::copy( U.begin(), U.end(), upper ) ;
  return 0 ;
}

/*!
  \return 0 ok otherwise the code ...
\*/
OCP_integer
OCP_get_algebraic_states_bounds( OCP_real lower[], OCP_real upper[] ) {
  vector<real> L, U ;
  pTest->get_algebraic_states_bounds( L, U ) ;
  std::copy( L.begin(), L.end(), lower ) ;
  std::copy( U.begin(), U.end(), upper ) ;
  return 0 ;
}

/*!
  \return 0 ok otherwise the code ...
\*/
OCP_integer
OCP_get_controls_bounds( OCP_real lower[], OCP_real upper[] ) {
  vector<real> L, U ;
  pTest->get_controls_bounds( L, U ) ;
  std::copy( L.begin(), L.end(), lower ) ;
  std::copy( U.begin(), U.end(), upper ) ;
  return 0 ;
}

/*!
  \return 0 ok otherwise the code ...
\*/
OCP_integer
OCP_get_discrete_control_values( OCP_integer i, OCP_real values[] ) {
  vector<real> values_vec ;
  pTest->get_discrete_control_values( i, values_vec ) ;
  std::copy( values_vec.begin(), values_vec.end(), values ) ;
  return 0 ;
}

OCP_integer
OCP_get_discrete_control_number_of_values( OCP_integer i, OCP_integer & number_of_values ) {
  number_of_values = pTest->get_discrete_control_number_of_values(i) ;
  return 0 ;
}

/*!
  \return 0 ok otherwise the code ...
\*/
OCP_integer
OCP_get_parameters_bounds( OCP_real lower[], OCP_real upper[] ) {
  vector<real> L, U ;
  pTest->get_parameters_bounds( L, U ) ;
  std::copy( L.begin(), L.end(), lower ) ;
  std::copy( U.begin(), U.end(), upper ) ;
  return 0 ;
}

/*!
  \return 0 ok otherwise the code ...
\*/
OCP_integer
OCP_get_boundary_conditions_bounds( OCP_real lower[], OCP_real upper[] ) {
  vector<real> L, U ;
  pTest->get_boundary_conditions_bounds( L, U ) ;
  std::copy( L.begin(), L.end(), lower ) ;
  std::copy( U.begin(), U.end(), upper ) ;
  return 0 ;
}

/*!
  \return 0 ok otherwise the code ...
\*/
OCP_integer
OCP_get_states_constraints_bounds( OCP_real lower[], OCP_real upper[] ) {
  vector<real> L, U ;
  pTest->get_states_constraints_bounds( L, U ) ;
  std::copy( L.begin(), L.end(), lower ) ;
  std::copy( U.begin(), U.end(), upper ) ;
  return 0 ;
}

/*!
  Return the value of Mayer term...
  
  \param computation_mode 1 compute only value
                          2 compute only gradient
                          3 compute both value and gradient
\*/
OCP_integer
OCP_mayer( OCP_real    const initial_state[],
           OCP_real    const final_state[],
           OCP_real    const parameters[],
           OCP_integer const computation_mode,
           OCP_real &        value,
           OCP_real          gradient[] ) {
  pTest->mayer( initial_state,
                final_state,
                parameters,
                computation_mode,
                value,
                gradient ) ;
  return 0 ;
}
  
// -----------------------------------------------------------------------------

/*!
  \return 0 ok otherwise the code ...
\*/
OCP_integer
OCP_lagrange( OCP_real    const differential_states[],
              OCP_real    const algebraic_states[],
              OCP_real    const controls[],
              OCP_real    const discrete_controls[],
              OCP_real    const parameters[],
              OCP_real          independent, // normally time
              OCP_integer const computation_mode,
              OCP_real &        value,
              OCP_real          gradient[] ) {
  pTest->lagrange( differential_states,
                   algebraic_states,
                   controls,
                   discrete_controls,
                   parameters,
                   independent,
                   computation_mode,
                   value,
                   gradient ) ;
  return 0 ;
}
// -----------------------------------------------------------------------------

/*!
  \return 0 ok otherwise the code ...
\*/
OCP_integer
OCP_rhs_differental_part_nnz( OCP_integer & nnz ) {
  nnz = pTest->rhs_differental_part_nnz() ;
  return 0 ;
}

/*!
  \return 0 ok otherwise the code ...
\*/
OCP_integer
OCP_rhs_differental_part_pattern( OCP_integer rows[],
                                  OCP_integer cols[],
                                  OCP_integer offset ) ;

/*!
  \return 0 ok otherwise the code ...
\*/
OCP_integer
OCP_rhs_differental_part( OCP_real    const differential_states[],
                          OCP_real    const algebraic_states[],
                          OCP_real    const controls[],
                          OCP_real    const discrete_controls[],
                          OCP_real    const parameters[],
                          OCP_real          independent, // normally time
                          OCP_integer const computation_mode,
                          OCP_real          rhs[],
                          OCP_real          jacobian_values_of_rhs[] ) ;
                          // differential + algebraic + controls + parameters + independent

// -----------------------------------------------------------------------------

/*!
  \return 0 ok otherwise the code ...
\*/
OCP_integer
OCP_rhs_algebraic_part_nnz( OCP_integer & nnz ) {
  nnz = pTest->rhs_algebraic_part_nnz() ;
  return 0 ;
}

/*!
  \return 0 ok otherwise the code ...
\*/
OCP_integer
OCP_rhs_algebraic_part_pattern( OCP_integer rows[],
                                OCP_integer cols[],
                                OCP_integer offset ) {
  pTest->rhs_algebraic_part_pattern( rows, cols, offset ) ;
  return 0 ;
}

/*!
  \return 0 ok otherwise the code ...
\*/
OCP_integer
OCP_rhs_algebraic_part( OCP_real    const differential_states[],
                        OCP_real    const algebraic_states[],
                        OCP_real    const controls[],
                        OCP_real    const discrete_controls[],
                        OCP_real    const parameters[],
                        OCP_real          independent, // normally time
                        OCP_integer const computation_mode,
                        OCP_real          rhs[],
                        OCP_real          jacobian_values_of_rhs[] ) ;
                        // differential + algebraic + controls + parameters + independent

// -----------------------------------------------------------------------------

/*!
  \return 0 ok otherwise the code ...
\*/
OCP_integer
OCP_lhs_mass_matrix_nnz( OCP_integer & nnz ) {
  nnz = pTest->lhs_mass_matrix_nnz() ;
  return 0 ;
}

/*!
  \return 0 ok otherwise the code ...
\*/
OCP_integer
OCP_lhs_mass_matrix_pattern( OCP_integer rows[],
                             OCP_integer cols[],
                             OCP_integer offset ) {
  pTest->lhs_mass_matrix_pattern( rows, cols, offset ) ;
  return 0 ;
}

/*!
  \return 0 ok otherwise the code ...
\*/
OCP_integer
OCP_lhs_mass_matrix( OCP_real const differential_states[],
                     OCP_real const parameters[],
                     OCP_real       independent, // normally time
                     OCP_real       values[] ) {
  pTest->lhs_mass_matrix( differential_states,
                          parameters,
                          independent,
                          values ) ;
  return 0 ;
}

/*!
  \return 0 ok otherwise the code ...
\*/
OCP_integer
OCP_lhs_adjoint_mass_matrix_derivative_nnz( OCP_integer & nnz ) {
  nnz = pTest->lhs_adjoint_mass_matrix_derivative_nnz() ;
  return 0 ;
}

/*!
  \return 0 ok otherwise the code ...
\*/
OCP_integer
OCP_lhs_adjoint_mass_matrix_derivative_pattern( OCP_integer rows[],
                                                OCP_integer cols[],
                                                OCP_integer offset ) {
  pTest->lhs_adjoint_mass_matrix_derivative_pattern( rows, cols, offset ) ;
  return 0 ;
}

/*!
  \return 0 ok otherwise the code ...
\*/
OCP_integer
OCP_lhs_adjoint_mass_matrix_derivative( OCP_real const differential_states[],
                                        OCP_real const parameters[],
                                        OCP_real       independent, // normally time
                                        OCP_real       values[] ) {
  pTest->lhs_adjoint_mass_matrix_derivative( differential_states,
                                             parameters,
                                             independent,
                                             values ) ;
  return 0 ;
}

// -----------------------------------------------------------------------------

/*!
  \return 0 ok otherwise the code ...
\*/
OCP_integer
OCP_states_constraints_nnz( OCP_integer & nnz ) {
  nnz = pTest->states_constraints_nnz() ;
  return 0 ;
}

/*!
  \return 0 ok otherwise the code ...
\*/
OCP_integer
OCP_states_constraints_pattern( OCP_integer rows[],
                                OCP_integer cols[],
                                OCP_integer offset ) {
  pTest->states_constraints_pattern( rows, cols, offset ) ;
  return 0 ;
}

/*!
  \return 0 ok otherwise the code ...
\*/
OCP_integer
OCP_states_constraints( OCP_real    const differential_states[],
                        OCP_real    const algebraic_states[],
                        OCP_real    const controls[],
                        OCP_real    const discrete_controls[],
                        OCP_real    const parameters[],
                        OCP_real          independent, // normally time
                        OCP_integer const computation_mode,
                        OCP_real          constraints[],
                        OCP_real          jacobian_values_of_constraints[] ) {
  pTest->states_constraints( differential_states,
                             algebraic_states,
                             controls,
                             discrete_controls,
                             parameters,
                             independent,
                             computation_mode,
                             constraints,
                             jacobian_values_of_constraints ) ;
  return 0 ;
}

// -----------------------------------------------------------------------------

/*!
  \return 0 ok otherwise the code ...
\*/
OCP_integer
OCP_boundary_conditions_nnz( OCP_integer & nnz ) {
  nnz = pTest->boundary_conditions_nnz() ;
  return 0 ;
}

/*!
  \return 0 ok otherwise the code ...
\*/
OCP_integer
OCP_boundary_conditions_pattern( OCP_integer rows[],
                                 OCP_integer cols[],
                                 OCP_integer offset ) {
  pTest->boundary_conditions_pattern( rows, cols, offset ) ;
  return 0 ;
}

/*!
  \return 0 ok otherwise the code ...
\*/
OCP_integer
OCP_boundary_conditions( OCP_real    const initial_state[],
                         OCP_real    const final_state[],
                         OCP_real    const parameters[],
                         OCP_integer const computation_mode,
                         OCP_real          values[],
                         OCP_real          jacobian_values_of_bc[] ) {
  pTest->boundary_conditions( initial_state,
                              final_state,
                              parameters,
                              computation_mode,
                              values,
                              jacobian_values_of_bc ) ;
  return 0 ;
}
